#include "main.h"
#include "stm32f4xx_hal.h"
#include "usb_device.h"
#include "stm32f4xx_hal_adc.h"
#include "stm32f4xx_hal_dac.h"
#include "usb_device.h"
#include "usbd_customhid.h"
#include "stdbool.h"
#include "math.h"

/*
 * Config Macros
 * 
 */
#define EN_BLINKY_
#define _TIMER2_INIT_
#define USB_T_COMPLEMENTARY_BUFFER_
#define DAC_UPDATE_IN_MAIN_LOOP

// DO not change fixed sampling rate (1/25)ms, CORRECTLY CONFIGURED CHECKED
#define TIMER5_PERIOD (0.001/SAMPLES_PER_MS)
// DO NOT CHANGE FIXED GENERATION RATE called every (10us), can update DAC 100 times in 1ms OR 50 times in 0.5ms OR ONCE every 10us. CORRECTLY CONFIGURED CHECK
#define TIMER2_PERIOD (0.00004)
#define pi                        (3.14159)

/* Signal generation MACROS */
#define SINE_FREQ_IN_HZ           (2000U)
#define SINE_WAVE_CONSTANT        ( 2 * pi * SINE_FREQ_IN_HZ)

// step change for t which in change updates the amplitude
#define dt (TIMER2_PERIOD)

#define USB_T_BUFFER_SIZE (64U)

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DAC_HandleTypeDef hdac;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim5;

/* USER CODE BEGIN PV */
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DAC_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM5_Init(void);

typedef struct{
  uint32_t ltick;  // last tick
  uint32_t period; // in milliseconds
  uint8_t kill;    // set to 0xFF to render it useless
}systick_sched_t;

typedef struct{
  union{
    uint32_t timestamp;
    struct{
      uint8_t timestamp_ar[4U];
    }timestamp_s;;
  }timestamp_u;
}timestamp_t;

typedef struct{
  union{
    uint16_t adc;
    struct{
      uint8_t adc_ar[2U];
    }adc_s;;
  }adc_u;
}adc_t;

#ifdef USB_T_COMPLEMENTARY_BUFFER_
typedef enum
{
  USB_T_BUFFER_DANGLING     = 0U,
  USB_T_BUFFER_INITIALIZED,
  USB_T_BUFFER_FILLING,
  USB_T_BUFFER_FILLED,
  // ready to transmit
  USB_T_BUFFER_RDT        = USB_T_BUFFER_FILLED,
  // redy to reuse
  USB_T_BUFFER_RTR        = USB_T_BUFFER_DANGLING

}USB_T_BUFFER_STATE_E;

typedef struct
{
  uint8_t count;
  USB_T_BUFFER_STATE_E state;
  uint8_t tbuffer[USB_T_BUFFER_SIZE];
}usb_t_buffer_t;

usb_t_buffer_t usb_t_buffer1;
usb_t_buffer_t usb_t_buffer2;
uint16_t adc_val;
uint8_t usb_tbuffer[USB_T_BUFFER_SIZE];
uint8_t * tbuffer_p = NULL;


void usb_t_buffer_init( usb_t_buffer_t * usb_t_buffer)
{
    usb_t_buffer->count = 4U;
     memset( (void *) usb_t_buffer->tbuffer, 0x0, USB_T_BUFFER_SIZE);
    usb_t_buffer->state = USB_T_BUFFER_INITIALIZED;
}
#endif

systick_sched_t dac_sched = {
  0U,
  1U, // 1usecond
  0xFF,
};

systick_sched_t adc_sched = {
  0U,
  0U, // ADC sampling RATE fixing it to 2KHz
  0x00,
};
systick_sched_t usb_sched = {
  0U,
  1U, //1ms systick
  0x00,
};

timestamp_t time;
adc_t analog;
double t = 0U;
const double SINE_TP = (double) ((double)1/SINE_FREQ_IN_HZ);

volatile ttick_t ttick2 = 0U;
volatile ttick_t ttick5 = 0U;

// called in tim2 callback, signal generation upto 2KHz, called every 50us, that gives 200 updates in 1ms
void DAC_update();
// called in tim5 callback every (0.4)ms which samples 25 ADC samples in 1ms FIXED.
void ADC_read();
// called in systick callback
void USB_send();

//called every 50us
void TIM2_Callback();
//called every (1/25) ms
void TIM5_Callback();
void delay(uint32_t delay)
{
  while(delay != 0U)
  {
    delay --;
  }
  return;
}

int main(void)
{
  HAL_StatusTypeDef status = HAL_OK;

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DAC_Init();
  MX_ADC1_Init();
  MX_USB_DEVICE_Init();

#ifdef USB_T_COMPLEMENTARY_BUFFER_
   usb_t_buffer_init(&usb_t_buffer1);
   usb_t_buffer_init(&usb_t_buffer2);
#endif

   /*  Start DAC */
   if(HAL_OK == status)
   status = HAL_DAC_Start(&hdac, DAC_CHANNEL_1);

   if(HAL_OK == status)
   status = HAL_ADC_Start(&hadc1);

#ifdef EN_BLINKY_
  GPIO_InitTypeDef  GPIOD_init_config;
  GPIOD_init_config.Pin  = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
  GPIOD_init_config.Mode = GPIO_MODE_OUTPUT_PP;
  GPIOD_init_config.Speed = GPIO_SPEED_FREQ_MEDIUM;

  __HAL_RCC_GPIOD_CLK_ENABLE();

  HAL_GPIO_Init(GPIOD, &GPIOD_init_config);
#endif

#ifdef _TIMER2_INIT_
  // TIMER2 DAC update
  MX_TIM2_Init();

  // TIMER 5 ADC read
  MX_TIM5_Init();


  HAL_TIM_Base_Start_IT(&htim2);
  HAL_TIM_Base_Start_IT(&htim5);
#endif

  /* Infinite loop */
  while (1)
  {
#ifndef EN_BLINKY_
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15, GPIO_PIN_SET);
    HAL_Delay(300);
    HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15, GPIO_PIN_RESET);
    HAL_Delay(300);
#endif
  }
  return (0);
}

#define LED_MASK_ON (1 << 13U)
#define LED_MASK_OFF (1 << 29U)
uint8_t flaggg = 0xFF;
// called every 50us
void TIM2_Callback()
{
#ifdef _TIMER2_INIT_
  DAC_update();
#endif /* _TIMER2_INIT_ */
}

// called every 1ms/25
void TIM5_Callback()
{
  #ifdef USB_T_COMPLEMENTARY_BUFFER_
    ADC_read();
  #endif /* USB_T_COMPLEMENTARY_BUFFER_ */
}

uint32_t dac_val = 0U;
uint32_t err_code_dac =0U;
// called every 10us
void DAC_update()
{
  HAL_StatusTypeDef status = HAL_OK;
  double angle = 0.0;

  t += dt;
  if( t >= (double) SINE_TP)
    t = 0;
  else
	  //do nothing

  angle = (double) SINE_WAVE_CONSTANT * t;
  dac_val = 2047 * (double)( (double)sin(angle) + 1U);

  status = HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, dac_val );
  if(HAL_OK != status)
    err_code_dac = 69;
}

uint64_t donut, spudnut;
//returns adc val
void ADC_read()
{
	donut = ttick2;
  adc_val= (uint16_t) HAL_ADC_GetValue(&hadc1);

  if (USB_T_BUFFER_FILLING ==  usb_t_buffer1.state || USB_T_BUFFER_INITIALIZED ==  usb_t_buffer1.state )
  {
    if(USB_T_BUFFER_FILLING !=usb_t_buffer1.state)
      usb_t_buffer1.state = USB_T_BUFFER_FILLING;

    usb_t_buffer1.tbuffer[usb_t_buffer1.count] =      ( (adc_val >> 0U) & 0xFF);
    usb_t_buffer1.tbuffer[usb_t_buffer1.count + 1U] = ( (adc_val >> 8U) & 0xFF);
    usb_t_buffer1.count+= 2U;
    if(USB_T_BUFFER_SIZE == usb_t_buffer1.count || USB_T_TRANSMIT_SIZE == usb_t_buffer1.count)
    {
      // buffer 1 filled, init buffer 2
      usb_t_buffer1.state = USB_T_BUFFER_FILLED;
      usb_t_buffer_init(&usb_t_buffer2);
    }
    else
    {
      //do no thing
    }

  }
  else if(USB_T_BUFFER_FILLING ==  usb_t_buffer2.state || USB_T_BUFFER_INITIALIZED ==  usb_t_buffer2.state )
  {
    if (USB_T_BUFFER_FILLING ==  usb_t_buffer2.state || USB_T_BUFFER_INITIALIZED ==  usb_t_buffer2.state )
    {
      if(USB_T_BUFFER_FILLING != usb_t_buffer2.state)
        usb_t_buffer2.state = USB_T_BUFFER_FILLING;

      usb_t_buffer2.tbuffer[usb_t_buffer2.count] =      ( (adc_val >> 0U) & 0xFF);
      usb_t_buffer2.tbuffer[usb_t_buffer2.count + 1U] = ( (adc_val >> 8U) & 0xFF);
      usb_t_buffer2.count+= 2U;
      if(USB_T_BUFFER_SIZE == usb_t_buffer2.count || USB_T_TRANSMIT_SIZE == usb_t_buffer2.count)
      {
        // buffer 1 filled, init buffer 2
        usb_t_buffer2.state = USB_T_BUFFER_FILLED;
        usb_t_buffer_init(&usb_t_buffer1);
      }
      else
      {
        //do no thing
      }
    }
  }
  spudnut = ttick2 - donut;
}

uint8_t usb_test_tbuffer[64] = {
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
		0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff


};

void USB_send()
{
  uint8_t status = 0;
  uint32_t ctick = HAL_GetTick();
  int jack = 0U;

  // if USB ready
  if (hUsbDeviceFS.dev_state == USBD_STATE_CONFIGURED )
  {
    time.timestamp_u.timestamp = ctick;

#ifdef USB_T_COMPLEMENTARY_BUFFER_
    if(USB_T_BUFFER_RDT == usb_t_buffer1.state)
    {
      tbuffer_p = usb_t_buffer1.tbuffer;
    }
    else if(USB_T_BUFFER_RDT == usb_t_buffer2.state)
    {
      tbuffer_p = usb_t_buffer2.tbuffer;
    }
    else
    {
    	jack = 69;
#ifndef _TEST_EN
      return;
#endif
    }
#endif /* USB_T_COMPLEMENTARY_BUFFER_ */

#ifndef USB_T_COMPLEMENTARY_BUFFER_
    tbuffer_p = usb_tbuffer;
    adc_val = ADC_read();
#endif /* !USB_T_COMPLEMENTARY_BUFFER_ */

#ifdef _TEST_EN
    tbuffer_p = usb_test_tbuffer;
#endif
    tbuffer_p[0] = time.timestamp_u.timestamp_s.timestamp_ar[0];
    tbuffer_p[1] = time.timestamp_u.timestamp_s.timestamp_ar[1];
    tbuffer_p[2] = time.timestamp_u.timestamp_s.timestamp_ar[2];
    tbuffer_p[3] = time.timestamp_u.timestamp_s.timestamp_ar[3];

#ifndef USB_T_COMPLEMENTARY_BUFFER_
    tbuffer_p[4] = (adc_val >> 0U) & 0xFF;
    tbuffer_p[5] = (adc_val >> 8U) & 0xFF;
#endif /* USB_T_COMPLEMENTARY_BUFFER_ */

    status = USBD_CUSTOM_HID_SendReport(&hUsbDeviceFS, tbuffer_p, USB_T_TRANSMIT_SIZE);
    if(0 != status)
      exit(1);
  }
  else
    return;
}

void HAL_SYSTICK_Callback(void)
{
  uint32_t ctick = HAL_GetTick();

  USB_send();
}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 84 * 1000000 * TIMER2_PERIOD; // to generate 1us
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM5 init function */
static void MX_TIM5_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 0;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 84 * 1000000 * TIMER5_PERIOD;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}
/* DAC init function */
static void MX_DAC_Init(void)
{

  DAC_ChannelConfTypeDef sConfig;

    /**DAC Initialization 
    */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**DAC channel OUT1 config 
    */
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif
