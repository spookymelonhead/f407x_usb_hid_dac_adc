#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hidapi.h"
#include "stdint.h"
#include <sys/time.h>
#include "string.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#define VendorID (0x0483)

#define ProductID (0x5750)

#define SerialNo (0x00000000001A)

#define TIMESTAMP_RES (0.04)

/* Config MACROS */
#define _USB_HID_TIMESTAMP_

#define _USB_DAC_ADC_20

#define _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE

#ifdef _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE
#define FILE_NAME "/home/brian/Desktop/plotfile.dat"
#endif /* _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE */

#ifdef _USB_DAC_ADC_1
	#ifdef _USB_DAC_ADC_30
		#error "ONE CONFIG MACRO ALLOWED AT A TIME"
	#endif
#endif

#ifdef _USB_DAC_ADC_30
	#ifdef _USB_DAC_ADC_1
		#error "ONE CONFIG MACRO ALLOWED AT A TIME"
	#endif
#endif

#ifdef _USB_DAC_ADC_20
	#ifdef _USB_DAC_ADC_30
		#error "ONE CONFIG MACRO ALLOWED AT A TIME"
	#endif
#endif

#ifdef _USB_DAC_ADC_30
	#ifdef _USB_DAC_ADC_20
		#error "ONE CONFIG MACRO ALLOWED AT A TIME"
	#endif
#endif

#ifdef _USB_DAC_ADC_1
	#ifdef _USB_HID_TIMESTAMP_
		#define HID_RECIEVE_DATA_SIZE (6U)
	#endif
#endif

#ifdef _USB_DAC_ADC_20
	#ifdef _USB_HID_TIMESTAMP_
		#define HID_RECIEVE_DATA_SIZE (54U)
	#endif
#endif

#ifdef _USB_DAC_ADC_30
	#ifdef _USB_HID_TIMESTAMP_
		#define HID_RECIEVE_DATA_SIZE (64U)
	#endif
#endif

#define HID_TRANSMIT_DATA_SIZE (0U)
#define _INPUT_REPORT_SIZE (4U)



typedef struct
{
	union
	{
		uint32_t timestamp;
		struct
		{
			uint8_t timestamp_ar[4U];
		}timestamp_s;;
	}timestamp_u;

}timestamp_t;


timestamp_t tame;

#define _USB_DAC_ADC_ 

#ifdef _USB_DAC_ADC_
typedef struct
{
	union
	{
		uint16_t adc;
		struct
		{
			uint8_t adc_ar[2U];
		}adc_s;;

	}adc_u;

}adc_t;


adc_t analog;

#endif /* _USB_DAC_ADC_ */

long long current_timestamp();
// Read requested state
uint64_t last_tick, curr_tick, diff;
const uint8_t usb_hid_transmit_buffer[64U] =  { 69,  2,  3,  4,  5,  6,  7,  8,  9,  10,
																								11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
																								21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
																								31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
																								41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
																								51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
																								61, 62, 63 
																							};

int main(int argc, char* argv[])
{
	int res;
	uint8_t buf[HID_RECIEVE_DATA_SIZE];

	hid_device *handle;
	int i;

#ifdef _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE
	/* FILE stuff */
	FILE * plotfile_p;
	plotfile_p = fopen(FILE_NAME, "w");
	if(NULL == plotfile_p)
	{
		printf("Failed to open the file\n");
		return (-1);
	}
	printf("File opened successfully\n");
	usleep(1000);
#endif /* _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE */

	// Enumerate and print the HID devices on the system
	struct hid_device_info *devs, *cur_dev;
	
	devs = hid_enumerate(0x00, 0x00);
	cur_dev = devs;	
	while (cur_dev) 
	{
		printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls",
		cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
		printf("\n");
		printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
		printf("  Product:      %ls\n", cur_dev->product_string);
		printf("\n");
		cur_dev = cur_dev->next;
	}
	hid_free_enumeration(devs);


	// Open the device using the VID, PID,
	// and optionally the Serial number.
	handle = hid_open(VendorID, ProductID, NULL);

	// Send a Feature Report to the device
	buf[0] = 0x2; // First byte is report number
	buf[1] = 0xa0;
	buf[2] = 0x0a;
	res = hid_send_feature_report(handle, buf, 17);

	// Read a Feature Report from the device
	buf[0] = 0x2;
	res = hid_get_feature_report(handle, buf, sizeof(buf));

	// Print out the returned buffer.
	printf("Feature Report\n   ");
	for (i = 0; i < res; i++)
		printf("%02hhx ", buf[i]);
	printf("\n");

	// Set the hid_read() function to be non-blocking.
	hid_set_nonblocking(handle, 1);

#ifdef _USB_DAC_ADC_

	uint32_t adc_val = 0U;
#endif

	while(1)
	{	
		memset(buf, 0x0, 64U);
		res = hid_read(handle, buf, HID_RECIEVE_DATA_SIZE);

		if (res < 0)
		{
			printf("Unable to read()\n");
			exit(0);
		}	
		else
		{
#ifdef _USB_HID_TIMESTAMP_
			tame.timestamp_u.timestamp_s.timestamp_ar[0] = buf[0];
			tame.timestamp_u.timestamp_s.timestamp_ar[1] = buf[1];
			tame.timestamp_u.timestamp_s.timestamp_ar[2] = buf[2];
			tame.timestamp_u.timestamp_s.timestamp_ar[3] = buf[3];
			printf("\nTimestamp%u\n",tame.timestamp_u.timestamp );
#endif /* _USB_HID_TIMESTAMP_ */

#ifdef _USB_DAC_ADC_1
			analog.adc_u.adc_s.adc_ar[0] = buf[4];
			analog.adc_u.adc_s.adc_ar[1] = buf[5];
			printf("\nanalog sample%u\n", analog.adc_u.adc_s.adc);
#endif /* _USB_DAC_ADC_1 */

#if defined (_USB_DAC_ADC_30) || defined (_USB_DAC_ADC_20)
			int counter;
			for(counter = 4U; counter <= HID_RECIEVE_DATA_SIZE - 2U; counter += 2U)
			{
				analog.adc_u.adc_s.adc_ar[0] = buf[counter];
				analog.adc_u.adc_s.adc_ar[1] = buf[counter + 1U];

				printf(" smpl%u: %u ", (counter/2U) - 1U , analog.adc_u.adc);

				if(0U != counter && 0U == counter % 5U)
					printf("\n");

#ifdef _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE
			fprintf(plotfile_p,"%lf\t\t",(double) ( tame.timestamp_u.timestamp + (double) TIMESTAMP_RES * ((counter/2U) - 1U -1U)) );
			fprintf(plotfile_p,"%u\n", (uint16_t) analog.adc_u.adc);
#endif /* _USB_DAC_ADC_SAMPLES_SAVE_TO_FILE */

			}

#endif /* _USB_DAC_ADC_30 */

#ifdef PRINT_RAW_BUFFER
			int counter;
			for(counter = 0U; counter < HID_RECIEVE_DATA_SIZE; counter += 2U)
			{
					printf("%u %u ", buf[counter] , buf[counter + 1U]);	
					printf("\n");					
			}		
#endif /* PRINT_RAW_BUFFER */

			// printf("\nSent");
			// hid_write(handle, usb_hid_transmit_buffer, 64);

			curr_tick = current_timestamp();
			diff = curr_tick - last_tick;
			last_tick = curr_tick;
			printf("\nRed Freq host:" "%"PRIu64"\n", diff);

			usleep(1000);
		}
	}
	return 0;
}



long long current_timestamp() 
{
    struct timeval te; 
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}