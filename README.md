# README #

Playing around with USB on STM32F407x Discovery board.

Pre-requisites:
STM32 HAL Libraries,
USB HID BASICS,
USB REPORT DESCRIPOTORS,
HIDAPI(I use Linux).

### What is this repository for? ###

* HID API is the class of USB used for Mouse and Keyboards so Human could interact/control a PC. 
* So I use a Timer to generate tick of ( 1/30 )ms why? because USB HID sends 64 Bytes in 1 report in every 1ms(via systick callback)..
* 4 bytes of timestamp and 60 bytes for ADC sampled data(2bytes each) that is 30samples. so I get 30KHz sample rate for ADC data on my linux machine.
* Version 0.1[closed]

### How do I get set up? ###
* STM32F407xVGT6 Discover board *
* Follw steps for HIDAPI over here- https://github.com/signal11/hidapi
* Short PA0 to PA4, one of them is DAC out and other one is ADC in.
* Import project, compile, and flash.
* Once flashed, "terminal command: lsusb -v", look for STMicroelectronics device, one of them would be ST-Link and other one should be Custom HID.
* Connect micro USB from discovery board to your linux machine.
* If you've set uped HID api, they should be in the shared libraries then on terminal change directory to F407x_USB_HID_DAC_ADC /hidapi/ and "terminal command: ./c_hidtest.sh"
* and Analog samples should appear magically on the terminal. 

### Who do I talk to? ###

* author cn: Brian doofus
* mail: hardik.mad@gmail.com
* blog: https://tachymoron.wordpress.com